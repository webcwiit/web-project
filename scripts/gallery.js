function myFunction(imgs) {
    var expandImg = document.getElementById("expandedImg");
    var imgText = document.getElementById("imgtext");
    expandImg.src = imgs.src;
    switch (imgs.alt) {
    case "nineArch":
        imgText.innerHTML = "<p>The Nine Arch Bridge also called the Bridge in the Sky, is a bridge in Sri Lanka. It is one of the best examples of colonial-era railway construction in the country.</p>";
        break;
    case "gangarama":
        imgText.innerHTML = "<p>Gangaramaya Temple is one of the most important temples in Colombo, Sri Lanka, being a mix of modern architecture and cultural essence. </p>";
        break;
    case "mirissa":
        imgText.innerHTML = "<p>Mirissa is a small town on the south coast of Sri Lanka, located in the Matara District of the Southern Province.</p>";
        break;
    case "sigiriya":
        imgText.innerHTML = "<p>Sigiriya or Sinhagiri is an ancient rock fortress located in the northern Matale District near the town of Dambulla in the Central Province, Sri Lanka.</p>";
        break;
    case "meemure":
        imgText.innerHTML = "<p>Meemure is a village with a population of about 400. It is located near the border between Kandy District and Matale District in the Knuckles Mountain Range. Meemure is one of the most remote villages in Sri Lanka with the only access via a 14 km trail from the town of Loolwatte.</p>";
    }
    expandImg.parentElement.style.display = "block";
}

function changeBackgroundColor(color) {
    var backColor = color.value;
    document.getElementById("bodyColor").style.backgroundColor = backColor;
}